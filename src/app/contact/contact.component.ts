import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Observable } from 'rxjs/internal/Observable';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AlertService} from 'ngx-alerts';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  contactForm: FormGroup;
  data = {};

  constructor(private formBuilder: FormBuilder, private http: HttpClient, private alertService: AlertService) {}


  ngOnInit() {
    this.contactForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email], this.forbiddenEmails],
      message: ['', [Validators.required, Validators.minLength(10)]]
  });
  }


  onSubmit() {

      // stop here if form is invalid
         this.data = {
          from: 'Portfolio <mailgun@sandboxae38d3c0335b4c488aeb12cfa94fba80.mailgun.org>',
          to: 'hridaysur@yahoo.co.in',
          subject: 'regarding contact',
          text: `this is mail from portfolio
                 FROM: ${this.contactForm.value.name} < ${this.contactForm.value.email} >
                 Message: ${this.contactForm.value.message}`
        };

        const httpOptions = {
          headers: new HttpHeaders({
            'content-type' : 'application/json'
          })
        };
        this.contactForm.reset();
        this.alertService.success('Message sent Successfully');
        this.http.post('https://pd52ufuz68.execute-api.us-east-2.amazonaws.com/stage/',
        this.data, httpOptions).toPromise()
        .then((message) => {
          console.log(message);
        })
        .catch(err => console.log(err));
  }

  forbiddenEmails(control: FormControl): Promise<any> | Observable<any> {
    const promise = new Promise<any>((resolve, reject) => {
      setTimeout(() => {
        if (control.value === 'test@test.com') {
          resolve({'emailIsForbidden': true});
        } else {
          resolve(null);
        }
      }, 1500);
    });
    return promise;
  }

}
