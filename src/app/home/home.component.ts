import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    myStyle: object = {};
    myParams: object = {};
    width = 100;
    height = 100;

  constructor() { }

  ngOnInit() {
    this.myStyle = {
      'position': 'fixed',
      'width': '100%',
      'height': '100%',
      'background-image' : 'url("../../assets/images/background.jpg")',
      'background-repeat': 'no-repeat',
      'background-size': 'cover',
      'background-position': '50% 50%',
      'z-index': -1,
      'top': 0,
      'left': 0,
      'right': 0,
      'bottom': 0,
  };

  this.myParams = {
    particles: {
        number: {
            value: 100,
        },
        color: {
            value: '#ff0000'
        },
        shape: {
            type: 'circle',
        },
      }
  };
}

}
