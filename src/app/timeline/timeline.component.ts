import { Component, OnInit , HostListener, ElementRef, Input, ViewChild} from '@angular/core';
import {ScrollEvent} from 'ngx-scroll-event';

import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.css'],
  animations: [
    trigger('scrollAnimation', [
      state('show', style({
        opacity: 1,
        transform: 'translateX(0)'
      })),
      state('hide',   style({
        opacity: 0,
        transform: 'translateX(-100%)'
      })),
      transition('show => hide', animate('700ms ease-out')),
      transition('hide => show', animate('700ms ease-in'))
    ])
  ]
})
export class TimelineComponent implements OnInit {
  state = 'hide';

  constructor(public el: ElementRef) { }

  ngOnInit() {
  }

}
