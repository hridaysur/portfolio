import { Component, OnInit } from '@angular/core';
import {  Router } from '@angular/router';

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.css']
})
export class NavigationBarComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onHome(){
    this.router.navigate(['/']);
  }

  onAbout(){
    this.router.navigate(['/about']);
  }

  onContact(){
    this.router.navigate(['/contact']);
  }

  onPortfolio(){
    this.router.navigate(['/portfolio']);
  }

}
