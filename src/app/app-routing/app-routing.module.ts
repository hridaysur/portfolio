import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Routes, RouterModule} from '@angular/router';

import { HomeComponent } from '../home/home.component';
import { AboutComponent } from '../about/about.component';
import { PortfolioComponent } from '../portfolio/portfolio.component';
import { ContactComponent } from '../contact/contact.component';


const appRoutes: Routes = [
  // {path: '', component:LandingPageComponent},
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent, data: {page: 'home'}},
  {path: 'about', component: AboutComponent, data: {page: 'about'}},
  {path: 'portfolio', component: PortfolioComponent, data: {page: 'portfolio'}},
  {path: 'contact', component: ContactComponent, data: {page: 'contact'}}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
