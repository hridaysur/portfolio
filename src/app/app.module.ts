import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ModalModule} from 'angular-custom-modal';
import {SwiperModule} from 'angular2-useful-swiper';

import { AppComponent } from './app.component';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { ContactComponent } from './contact/contact.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import {ParticlesModule} from 'angular-particle';
import { ProjectDetailsComponent } from './project-details/project-details.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AlertModule} from 'ngx-alerts';
import { TimelineComponent } from './timeline/timeline.component';
import {ScrollEventModule} from 'ngx-scroll-event';
import {AnimateOnScrollModule} from 'ng2-animate-on-scroll';


@NgModule({
  declarations: [
    AppComponent,
    NavigationBarComponent,
    HomeComponent,
    AboutComponent,
    PortfolioComponent,
    ContactComponent,
    ProjectDetailsComponent,
    TimelineComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ParticlesModule,
    BrowserAnimationsModule,
    ModalModule,
    ReactiveFormsModule,
    HttpClientModule,
    AlertModule.forRoot({maxMessages: 5, timeout: 5000}),
    ScrollEventModule,
    AnimateOnScrollModule.forRoot(),
    SwiperModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
